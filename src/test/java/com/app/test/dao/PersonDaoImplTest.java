package com.app.test.dao;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.dao.PersonDao;
import com.app.entity.Person;
import com.app.test.utils.builders.PersonBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "file:src/main/resources/test-datasource.xml" })
public class PersonDaoImplTest {

	@Autowired
	PersonDao personDaoImpl;
	
	PersonBuilder personBuilder;

	@Test
	public void insertPerson() {
		//given
		personBuilder = new PersonBuilder();
		Person person = personBuilder.WithName("Janusz").build();

	    //when
		personDaoImpl.save(person);
		person = personDaoImpl.findById(person.getId());

	    //then
		Assert.assertNotNull(person.getId());
		Assert.assertEquals("Janusz", person.getName());
	}
	
	@Test
	public void updatePerson() {
		//given
		personBuilder = new PersonBuilder();
		Person updatedPerson = null;
		Person person = personBuilder.WithName("Max").WithOpinion("jest ok").build();

	    //when
		personDaoImpl.save(person);
		person = personDaoImpl.findById(person.getId());
		person.setName("Krzysztof");
		person.setSurname("Kowalewski");
		person.setCountry("Francja");
		person.setOpinion("Dobry pracownik");
		
		personDaoImpl.update(person);
		
		updatedPerson = personDaoImpl.findById(person.getId());

	    //then
		Assert.assertEquals(person.getId(), updatedPerson.getId());
		Assert.assertEquals(person.getName(), updatedPerson.getName());
		Assert.assertEquals(person.getSurname(), updatedPerson.getSurname());
		Assert.assertEquals(person.getCountry(), updatedPerson.getCountry());
		Assert.assertEquals(person.getOpinion(), updatedPerson.getOpinion());
	}
	
	@Test
	public void findByName() {
		//given
		personBuilder = new PersonBuilder();
		Person person = personBuilder.WithName("Janusz").build();

	    //when
		personDaoImpl.save(person);
		List<Person> list =  personDaoImpl.findByName(person.getName());

	    //then
		Assert.assertNotSame(0, list.size());
	}

	@Test
	public void deletePerson() {
		//given
		personBuilder = new PersonBuilder();
		Person person = personBuilder.WithName("Pawel").WithCountry("Kanada").build();

	    //when
		int id = personDaoImpl.save(person);
		personDaoImpl.delete(person);
		person = personDaoImpl.findById(person.getId());

	    //then
		Assert.assertNull(person);

	}

}
