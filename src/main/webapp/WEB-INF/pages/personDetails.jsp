<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Person Edit</title>
</head>
<body>
 <center>

  <div style="color: teal; font-size: 30px">Person Edit</div>



  <c:url var="userRegistration" value="saveUser.html" />
  <form:form id="registerForm" modelAttribute="person" method="post" action="update">
  <form:errors path="*" cssClass="errorblock" element="div" />
   <table width="400px" height="150px">
<%--    <c:if test="${!empty personList}"> --%>
   	<tr style="visibility: hidden;">
     <td><form:label path="id">id</form:label></td>
     <td><form:input path="id" value="${person.id}" /></td>
    </tr>
   <%--  </c:if> --%>
    <tr>
     <td><form:label path="name">First Name</form:label></td>
     <td><form:input path="name" value="${person.name}" /></td>
      <td><form:errors path="name" cssclass="error"></form:errors></td>
    </tr>
    <tr>
     <td><form:label path="surname">Last Name</form:label></td>
     <td><form:input path="surname" value="${person.surname}"/></td>
      <td><form:errors path="surname" cssclass="error"></form:errors></td>
    </tr>
    <tr>
     <td><form:label path="country">Country</form:label></td>
     <td><form:input path="country" value="${person.country}"/></td>
      <td><form:errors path="country" cssclass="error"></form:errors></td>
    </tr>
    <tr>
     <td><form:label path="opinion">Opinion</form:label></td>
     <td><form:input path="opinion" value="${person.opinion}"/></td>
      <td><form:errors path="opinion" cssclass="error"></form:errors></td>
    </tr>
    <tr>
     <td></td>
     <td><input type="submit" value="Zapisz" />
     </td>
    </tr>
   </table>
  </form:form>

 </center>
</body>
</html>


