<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista osób</title>
</head>
<body>
 <center>

  <div style="color: teal; font-size: 30px">Lista osób</div>

	<br/>
  <a href="persons/new">Dodaj nową osobę</a>
  <c:if test="${!empty personList}">
   <table border="1" bgcolor="black" width="600px">
    <tr
     style="background-color: teal; color: white; text-align: center;"
     height="40px">
     
     <td>Imię</td>
     <td>Nazwisko</td>
     <td>Państwo</td>
     <td>Opinia</td>
     <td>Edycja</td>
     <td>Kasowanie</td>
    </tr>
    <c:forEach items="${personList}" var="person">
     <tr
      style="background-color: white; color: black; text-align: center;"
      height="30px">
      
      <td><c:out value="${person.name}" />
      </td>
      <td><c:out value="${person.surname}" />
      </td>
      <td><c:out value="${person.country}" />
      </td>
      <td><c:out value="${person.opinion}" />
      </td>
      <td><a href="persons/edit?id=${person.id}">Edytuj</a></td>
      <td><a href="persons/delete?id=${person.id}">Usuń</a></td>
     </tr>
    </c:forEach>
   </table>
  </c:if>

 </center>
</body>
</html>



