package com.app.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.app.entity.Person;

public class PersonValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return Person.class.equals(arg0);
	}

	@Override
	public void validate(Object obj, Errors e) {
		 ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
		
	}

}
