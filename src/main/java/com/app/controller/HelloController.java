package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.dao.PersonDao;
import com.app.dao.impl.PersonDaoImpl;
import com.app.entity.Person;

@Controller
@RequestMapping("/welcome")
public class HelloController {
	
	@Autowired
	private PersonDao personDaoImpl;
 
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		
		model.addAttribute("message", "Spring 3 MVC, Dzien dobry");
		return "hello";
 
	}
	
	@RequestMapping(value = "/u")
	public String printWelcome2(@RequestParam String name, ModelMap model) {
		
		model.addAttribute("message", "Witamy serdecznie");
		model.addAttribute("name", name);
		return "welcome";
 
	}

 
}
