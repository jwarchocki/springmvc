package com.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.dao.PersonDao;
import com.app.entity.Person;

@Controller
@RequestMapping("/persons")
public class PersonController {

	@Autowired
	private PersonDao personDaoImpl;


	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView personList() {
		return new ModelAndView("personList", "personList",
				personDaoImpl.findAll());
	}

	@RequestMapping(value = "/new")
	public ModelAndView newPerson(@ModelAttribute Person person) {

		return new ModelAndView("personDetails");
	}
	
	@RequestMapping(value = "/delete")
	public ModelAndView deletePerson(@RequestParam int id) {
		personDaoImpl.deleteById(id);
		
		return new ModelAndView("redirect:/persons");
	}

	@RequestMapping(value = "/edit" )
	public ModelAndView editPerson(
			@ModelAttribute  Person employee, HttpServletRequest request) {
		
		return new ModelAndView("personDetails", "person",
				personDaoImpl.findById(Integer.valueOf(request.getParameter("id"))));
	}

	@RequestMapping(value = "/update")
	public ModelAndView updatePerson( @Valid Person person ,BindingResult bindingResult ) {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("personDetails");
		}
		else 
			personDaoImpl.saveOrUpdate(person);
		
		return new ModelAndView("redirect:/persons");
	}

}
