package com.app.test.utils.builders;

import com.app.entity.Person;

public class PersonBuilder {
	
	private int id;
	private String name = "Jan";
	private String surname = "Kowalski";
	private String country = "Polska";
	private String opinion = "Jest ok";

	 public   Person build(String name, String surname) {
	        return new Person(name, surname, country,opinion);
	    }
	 public  Person build() {
		 return new Person(name, surname, country,opinion);
	 }
	 
	 public PersonBuilder WithName(String name) {
		 this.name = name;
		 return this;
	 }
	 public PersonBuilder WithSurname(String surname) {
		 this.surname = surname;
		 return this;
	 }
	 public PersonBuilder WithCountry(String country) {
		 this.country = country;
		 return this;
	 }
	 public PersonBuilder WithOpinion(String opinion) {
		 this.opinion = opinion;
		 return this;
	 }
	 public PersonBuilder WithId(int id) {
		 this.id = id;
		 return this;
	 }
	 
}
