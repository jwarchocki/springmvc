package com.app.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

public interface GenericDao <E, PK extends Serializable> {
	PK save(E newInstance);
	void update(E transientObject);
	void saveOrUpdate(E transientObject);
	void delete (E peristanceObject);
	void deleteById(PK id);
	Session currentSession();
	Session openSession();
	E findById(PK id);
	List<E> findAll();
	List<E> findByPagination(int firstResult, int maxResults);
}