package com.app.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.app.dao.PersonDao;
import com.app.entity.Person;

@Repository
public class PersonDaoImpl extends GenericDaoImpl<Person, Integer> implements PersonDao {

	public PersonDaoImpl() {
		entityClass = Person.class;
	}

	@Override
	public List<Person> findByName(String name) {
		Session session = this.sessionFactory.openSession();
		List<Person> list = (List<Person>) session.createQuery("from " + getEntityClass().getName() + " where name = :name").setParameter("name", name).list();
		
		session.close();
		
		return list;
	}
}
