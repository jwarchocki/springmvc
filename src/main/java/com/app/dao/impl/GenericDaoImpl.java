package com.app.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.GenericDao;

@Transactional 
@Repository
public abstract class GenericDaoImpl <E, PK extends Serializable> implements GenericDao<E, PK> {
	
	@Autowired
    protected SessionFactory sessionFactory;
	protected Class<E> entityClass;
	
	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Session openSession() {
		return sessionFactory.openSession();
	}

	public Class<E> getEntityClass() {
		return entityClass;
	}

	@SuppressWarnings("unchecked")
	public PK save(E newInstance) {
		Transaction transaction = currentSession().beginTransaction();
		PK pk = (PK) currentSession().save(newInstance);
		transaction.commit();
		
		return pk;
	}

	public void update(E transientObject) {
		Transaction transaction = currentSession().beginTransaction();
		currentSession().update(transientObject);
		transaction.commit();

	}
	
	public void saveOrUpdate(E transientObject) {
		Transaction transaction = currentSession().beginTransaction();
		currentSession().saveOrUpdate(transientObject);
		transaction.commit();
	}

	public void delete(E peristanceObject) {
		Transaction transaction = currentSession().beginTransaction(); 
		currentSession().delete(peristanceObject);
		transaction.commit();
	}

	@SuppressWarnings("unchecked")
	public E findById(PK id) {
		E e = (E) openSession().get(getEntityClass(), id);
		currentSession().close();
		
		return e;
	}
	
	public void deleteById(PK id) {
		Transaction transaction = currentSession().beginTransaction(); 
		currentSession().createQuery("delete " + getEntityClass().getName() + " where id= :id").setParameter("id", id).executeUpdate();
		transaction.commit();
	}

	@SuppressWarnings("unchecked")
	public List<E> findAll() {
		Session session = this.sessionFactory.openSession();
		List<E> list = session.createQuery("from " + getEntityClass().getName() + " order by id").list();
		session.close();
		
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<E> findByPagination(int firstResult, int maxResults) {
		Session session = this.sessionFactory.openSession();
		List<E> list = session.createQuery("from " + getEntityClass().getName()).
				setFirstResult(firstResult).
				setMaxResults(maxResults).
				list();
		
		session.close();
		
		return list;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
