package com.app.dao;

import java.util.List;

import com.app.entity.Person;

public interface PersonDao extends GenericDao <Person, Integer>  {
	public List<Person> findByName(String name);

}
