package com.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity
public class Person {
	
	public Person() {
		// TODO Auto-generated constructor stub
	}
	
	public Person(String name , String surname) {
		this.name = name;
		this.surname = surname;
		
	}
	public Person(String name , String surname, String country,String opinion) {
		this.name = name;
		this.surname = surname;
		this.country = country;
		this.opinion = opinion;
		
	}
 
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
     
    @Column(name="name", nullable = false)
    @Size(min=2, max=50) 
    private String name;
    
    @Column(name="surname", nullable = false)
    @Size(min=2, max=50) 
    private String surname;
    
    @Column(name="country")
    private String country;
    
    @Column(name="opinion")
    private String opinion;
    
    public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOpinion() {
		return opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

 
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getCountry() {
        return country;
    }
 
    public void setCountry(String country) {
        this.country = country;
    }
     
    @Override
    public String toString(){
        return "id="+id+", name="+name+", country="+country;
    }
}
